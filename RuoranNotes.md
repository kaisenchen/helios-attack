Helios Attack - Ruoran notes
============================

Helios 2.0 a Django Web App
---------------------------

`settings.py`

SERVER_HOST -> javascript API HOST -> same origin policy

`a2um.com`



1. uri elections/NUM/vote
-------------------------

After django returned the response, control is transferred to javascript (Front-end).

`http://localhost:8000/elections/4/?date=1363376737859`
An AJAX GET will hit end point `/elections/4/`, which will return election info as a JSON obj.




BOOTH is an global object that contains most of the election, and vote, information.
BOOTH will be initialized after the page is load.

* `BOOTH.election`
* `BOOTH.ballot`
* `BOOTH.encrypted_ballot`

### BOOTH.election obj


```
{
  ballot_type: "homomorphic",

  election_hash: "L9C01BLLtczm6b1gsk7CV47MioI",

  election_id: 4,

  name: "Teazaea",

  openreg: undefined ,

  pk: { p: ... , q: ... , g: ... , y: ... },

  questions: ... ,
    
  tally_type: "homomorphic",        
  
  voters_hash: "19TyQw2reP2hvS4qRx5gFDbqSo8",
  
  voting_ends_at: null,

  voting_starts_at: null
}
```

### BOOTH.encrypted_ballot obj

```
{
  election: obj,
  election_hash: "L9C01BLLtczm6b1gsk7CV47MioI",
  election_id: 4,
  encrypted_answers: [ANSWERS]
}
```

### encrypted_answers

```
{
  answer: … ,
  choices: … ,
  individual_proofs: … ,
  overall_proof: … ,
  randomness: … 
}
```

url: `http://localhost:8000/elections/4/vote`

Controller: `helios/views.py`

```
@election_view(frozen=True)
def one_election_vote(request, election):
  """
  UI to vote in an election
  """
  return render_template(request, "vote", {'election': election})
```

Template: `helios/template/vote.html`


The statements bellow are jQuery jTemplates syntax.

```
BOOTH.setup_templates = function() {
    $('#election_div').setTemplateURL("/static/templates/booth/election.html");
    $('#question_div').setTemplateURL("/static/templates/booth/question.html");
    $('#confirm_div').setTemplateURL("/static/templates/booth/confirm.html");
    $('#seal_div').setTemplateURL("/static/templates/booth/seal.html");
    $('#audit_div').setTemplateURL("/static/templates/booth/audit.html");
    $('#login_div').setTemplateURL("/static/templates/booth/login.html");
    $('#done_div').setTemplateURL("/static/templates/booth/done.html");
};
```

`helios/template/booth.html` is imported by `helios/template/vote.html`

where imported the jQuery jTemplates

`<script language="javascript" src="/static/jquery-jtemplates.js"></script>`

`elections/4/vote`

##### 1. Start 

`#question_div` will display, and user can vote.

##### 2. Click Review all Choices

`#confirm_div` will display, and user can encrypt his choice.

##### 3. Click Encrypt

`#processing_div`, skimmed over.

`#seal_div` shows up. User can go to next step in order to submit this encrypted ballot to server. 
Or they could audit it via `#audit_div`

##### 4. Click Submit Encrypted Ballot

`#login_div` shows up asking you for email, password. Only selected voters could pass this step.

##### 5. Click send
ballot sent, `#done_div` displayed.

### More about send

`BOOTH.send_ballot_raw` will be called.

1. Create a payload. `data['encrypted_vote'] = jQuery.toJSON(BOOTH.encrypted_ballot); `

2. Verify user using AJAX. `/elections/{{election.election_id}}/get_voter_by_email`

3. Submit payload `/elections/{{election.election_id}}/voters/" + voter.voter_id + "/submit"`

Since BOOTH is exposed, maclicous javascript code could potentially modify the ballot and this submitting process.





## Issues Encountered 

1. Trying to access global variable BOOTH from firefox extension.

2.  


## Reference

1. Windows XP Service Pack 0 or upper, 

2. Firefox version 1.5 to 3.5.*,

3. Firefox installation folder is under Program Files,

4. The client must have write privilege for the mentioned 
folders,

5. Adobe Acrobat/Reader with versions 7.0.0 to 8.1.2 and 
9.0.0, is installed on the client’s machine
